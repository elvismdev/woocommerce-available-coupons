// Cross-Origin Request - This can be fixed by moving the resource to the same domain or enabling CORS.

(function () {
    var wooApp = angular.module('wooApp', ['ng.deviceDetector', 'angularAddToHomeScreen']);

    angular.module('wooApp').factory('wooAppFactory', function($http) {
        function requests() {

            // COUPONS
            // HTTPS
            $http.get(witnationCoupons.http_method + witnationCoupons.domain + witnationCoupons.request + witnationCoupons.filter, {
                params: {
                    'consumer_key': witnationCoupons.consumer_key,
                    'consumer_secret': witnationCoupons.customer_secret
                }
            }).success(function (data, status) {
                witnationCoupons.result.response = data;
                witnationCoupons.result.status = status;
            }).error(function (data, status, headers, config) {
                witnationCoupons.result.response = data.errors[0];
                witnationCoupons.result.status = status;
            });


            // COUPONS
            // HTTP
            cardoneCoupons.oauth.oauth_nonce = Math.random().toString(36).slice(2);
            var cardone_oauth_signature = getOauthSignature(cardoneCoupons);
            $http.get(cardoneCoupons.http_method + cardoneCoupons.domain + cardoneCoupons.request + cardoneCoupons.filter, {
                params: {
                    'oauth_consumer_key': cardoneCoupons.oauth.oauth_consumer_key,
                    'oauth_timestamp': cardoneCoupons.oauth.oauth_timestamp,
                    'oauth_nonce': cardoneCoupons.oauth.oauth_nonce,
                    'oauth_signature': cardone_oauth_signature,
                    'oauth_signature_method': cardoneCoupons.oauth.oauth_signature_method
                }
            }).success(function (data, status) {
                cardoneCoupons.result.response = data;
                cardoneCoupons.result.status = status;
            }).error(function (data, status, headers, config) {
                cardoneCoupons.result.response = data.errors[0];
                cardoneCoupons.result.status = status;
            });
        }

        return {
         requests: requests
     };
 });

wooApp.run(function (wooAppFactory) {
    wooAppFactory.requests();
});

wooApp.controller('wooAppCtrl', function ($scope, $interval, wooAppFactory) {

    var couponsWit = null;
    var couponsGc = null;

    $scope.now = today.toTimeString();
    $scope.witnationCoupons_error = '';
    $scope.cardoneCoupons_error = '';

    $interval(function () {
        $scope.now = new Date().toTimeString();
        wooAppFactory.requests();
        }, 60000); // 1 minute update time

    $scope.witnationCouponsResult = function () {
        if (witnationCoupons.result.status != 200) {
            if (witnationCoupons.result.response) {
                $scope.witnationCoupons_error = 'Error ocurred: ' + ' ' + witnationCoupons.result.status + ' ' + witnationCoupons.result.response.code + ' ' + witnationCoupons.result.response.message;
                couponsWit = 'NO COUPONS';
            }
        } else {
            if (witnationCoupons.result.response.coupons) {
                $scope.witnationCoupons_error = '';
                witnation_coupons = witnationCoupons.result.response.coupons;
                var couponsWit = [];
                for (var key in witnation_coupons) {
                    if (witnation_coupons[key]['usage_limit'] != 1 && testCodes.indexOf(witnation_coupons[key]['code'])) {
                        couponsWit.push(witnation_coupons[key]);
                    }
                }
                witnationCoupons.result.response.coupons = null; // Avoid 2 times iterations
            } else {
                $scope.witnationCoupons_error = 'Some error ocurred triying to get API info. Check for URL parameters. '  + witnationCoupons.result.response.errors[0].message;
                couponsWit = 'NO COUPONS';
            }
        }

        return couponsWit;
    };

    $scope.cardoneCouponsResult = function () {
        var cardone_coupons = 'NO COUPONS';

        if (cardoneCoupons.result.status != 200) {
            if (cardoneCoupons.result.response) {
                $scope.cardoneCoupons_error = 'Error ocurred: ' + ' ' + cardoneCoupons.result.status + ' ' + cardoneCoupons.result.response.code + ' ' + cardoneCoupons.result.response.message;
            }
        } else {
            if (cardoneCoupons.result.response.coupons) {
                $scope.cardoneCoupons_error = '';
                cardone_coupons = cardoneCoupons.result.response.coupons;
                cardoneCoupons.result.response.coupons = null; // Avoid 2 times iterations
            } else {
                $scope.cardoneCoupons_error = 'Some error ocurred triying to get API info. Check for URL parameters. ' + cardoneCoupons.result.response.errors[0].message;
            }
        }

        return cardone_coupons;
    };

});
})();